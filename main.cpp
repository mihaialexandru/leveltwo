#include <iostream>

int main()
{
	static size_t count = 150;
	std::cout << "Hello by Lasagna team!";
	if (count-- == 0)
	{
		return 0;
	}
	return main();
}
